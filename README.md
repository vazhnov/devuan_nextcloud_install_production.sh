Nextcloud interactive installer.

I will try to make it work in Devuan 4 Chimaera (without Systemd and netplan).

To get rid of:

* `localectl`
* `systemctl`
* `netplan`

## Current status

It works, with some warnings.

## Options

From original script:

* `--provisioning`, `export PROVISIONING=1` — do not ask questions?
* `--not-latest`, `export NOT_LATEST=1` — "not-latest mode"?
* `DEBUG=0` — have to be changed in the code;

Additionally:

* `DO_NOT_REMOVE=1` — do not remove files and packages,
* `CHANGE_NETWORK=0` — do not change network settings,

## External requests

Original script makes requests to:

* https://raw.githubusercontent.com/nextcloud/vm/master/lib.sh
* https://raw.githubusercontent.com/nextcloud/vm/master/static/adduser.sh
* https://github.com/nextcloud/vm/tree/master/geoblockdat
* https://download.nextcloud.com/server/releases
* https://api64.ipify.org
* 9.9.9.9 and 149.112.112.112 — Quad9 DNS servers

## Copyrights

Original project: https://github.com/nextcloud/vm/

License: GPL v3.
